<?php 
require_once('../../private/initialize.php');
$page_title = 'List of Drawings';
include(SHARED_PATH . '/cms_header.php');
$drawings_set = get_all_drawings();
?>

<body>
  <div class="container mt-3">
    <div style="display:flex; align-items:flex-end; justify-content:space-between;">
      <h1>List of all drawings</h1>
      <p><?php echo(mysqli_num_rows($drawings_set)); ?> tekeningen</p>
    </div>
    <table class="table">
      <thead>
        <tr>
          <th scope="col">Title</th>
          <th scope="col">Description</th>
          <th scope="col" style="text-align: right;">Date</th>
          <th scope="col" style="text-align: right;">Longitude</th>
          <th scope="col" style="text-align: right;">Latitude</th>
        </tr>
      </thead>
      <tbody>

<?php 
  while ($row = mysqli_fetch_assoc($drawings_set)) {
    $title = $row['title'];
    $title = strlen($title) > 50 ? substr($title, 0, 50) . '...' : $title;
    $description = $row['description'];
    $description = strlen($description) > 30 ? substr($description, 0, 30) . '...' : $description;
    echo '<tr>';
    echo '<td><a href="' . url_for('backend/edit.php') . '?id=' . $row['id'] . '">' . $title . '</a></td>';
    echo '<td><a href="' . url_for('backend/edit.php') . '?id=' . $row['id'] . '">' . $description . '</a></td>';
    echo '<td align="right">' . $row['date'] . '</td>';
    echo '<td align="right">' . $row['longitude'] . '</td>';
    echo '<td align="right">' . $row['latitude'] . '</td>';
    echo '</tr>';
  }
?>

      </tbody>
    </table>
  </div>
</body>
</html>

<?php db_disconnect($db); ?>
